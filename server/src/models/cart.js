const mongoose = require("mongoose");

const CartSchema = new mongoose.Schema({
  userId: String,
  products: [
    {
      productId: mongoose.Schema.Types.ObjectId,
      quantity: Number,
    },
  ],
});

const Cart = mongoose.model("Cart", CartSchema);

module.exports = Cart;
