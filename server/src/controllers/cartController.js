const Cart = require("../models/cart");

const getCart = async (req, res) => {
  try {
    const cart = await Cart.findOne({});
    if (!cart) {
      return res.status(404).json({ msg: "Cart not found" });
    }
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

const addToCart = async (req, res) => {
  try {
    let cart = await Cart.findOne({});
    if (!cart) {
      cart = new Cart({ products: [] });
    }
    const productIndex = cart.products.findIndex((p) => p.productId.equals(req.body.productId));
    if (productIndex > -1) {
      cart.products[productIndex].quantity += req.body.quantity;
    } else {
      cart.products.push({ productId: req.body.productId, quantity: req.body.quantity });
    }
    await cart.save();
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

const removeFromCart = async (req, res) => {
  try {
    const cart = await Cart.findOne({});
    if (!cart) {
      return res.status(404).json({ msg: "Cart not found" });
    }
    cart.products = cart.products.filter((p) => !p.productId.equals(req.body.productId));
    await cart.save();
    res.status(200).json(cart);
  } catch (error) {
    res.status(500).json({ msg: error.message });
  }
};

module.exports = {
  getCart,
  addToCart,
  removeFromCart,
};
