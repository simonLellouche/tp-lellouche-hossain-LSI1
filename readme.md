## Documentation : Prérequis pour faire fonctionner le projet

Cette documentation décrit les prérequis nécessaires pour configurer et exécuter le projet, qui comprend à la fois les composants serveur et frontend. Ce TP a été réalisé par le binôme Simon LELLOUCHE et Bushra HOSSAIN en L3 LSI1.

### Prérequis pour le Serveur

1. **Node.js, npm et MongoDB** :

   - Assurez-vous que Node.js, npm et MongoDB sont installés sur votre machine.

2. **Dépendances Node.js** :
   - Les dépendances nécessaires pour le projet sont spécifiées dans le fichier `/server/package.json`.
   - Allez dans le répertoire /server depuis la racine du projet avec la commande :
     ```sh
     cd server
     ```
   - Installez les dépendances en exécutant la commande suivante dans ce même répertoire :
     ```sh
     npm install
     ```

### Prérequis pour le Frontend

1. **Vue.js** :

   - Le frontend utilise Vue.js. Assurez-vous que Vue CLI est installé.
   - Vous pouvez installer Vue CLI globalement avec npm :
     ```sh
     npm install -g @vue/cli
     ```

2. **Dépendances du Frontend** :
   - Les dépendances nécessaires pour le frontend sont spécifiées dans le fichier `/front/package.json`.
     - Allez dans le répertoire /front depuis la racine du projet avec la commande :
     ```sh
     cd front
     ```
   - Installez-les en exécutant la commande suivante dans le répertoire frontend :
     ```sh
     npm install
     ```

### Configuration et Lancement du Serveur

1. **Connexion à la base de données** :

   - Assurez-vous que MongoDB est en cours d'exécution.
   - Le fichier `db.js` gère la connexion à MongoDB et génère des produits ainsi qu'un panier vide en BDD si celle-ci est vide. Ceci évite l'insertion manuelle des produits en BDD grâce à la logique de seeding.

2. **Exécution du Serveur** :

   - Pour démarrer le serveur, assurez-vous d'être dans le répertoire `/server` et exécutez la commande suivante :
     ```sh
     npm run start
     ```

3. **Endpoints du Serveur** :
   - Le serveur expose des endpoints pour les produits et le panier.
   - Les routes pour les produits sont définies dans `routes/productRoutes.js`.
   - Les routes pour le panier sont définies dans `routes/cartRoutes.js`.

### Configuration et Lancement du Frontend

1. **Modification du Frontend pour Ajuster les Quantités** :

   - Nous avons ajouté des boutons "+" et "-" pour ajuster la quantité des produits dans le panier, en utilisant Bootstrap pour le style.
   - Nous avons également fait en sorte que le bouton `supprimer` supprime totalement le produit du panier peu importe sa quantité.
   - Le fichier `IndexPage.vue` et `manager.js` ont été modifiés pour inclure ces boutons et la logique correspondante.

2. **Exécution du Frontend** :

   - Pour démarrer le client, assurez-vous d'être dans le répertoire `/front` et exécutez la commande suivante :
     ```sh
     npm run serve
     ```

3. **Accès à l'Application** :
   - L'application sera accessible à l'adresse suivante par défaut : `http://localhost:8080`.

### Fonctionnalités Utilisateur

1. **Gestion des Produits** :

   - Les produits sont récupérés dynamiquement depuis la base de données MongoDB grâce à l'API, au lieu d'être codés en dur côté frontend.

2. **Seeding de la Base de Données** :

   - Lors de la première connexion, la base de données est initialisée avec des produits par défaut et un panier vide grâce à la logique de seeding définie dans `db.js`.

3. **Gestion du Panier** :
   - Le panier est également enregistré en base de données, permettant une persistance des données utilisateur entre les sessions.
   - L'utilisateur peut ajouter des produits au panier, ajuster les quantités avec les boutons "+" et "-", et supprimer directement des produits du panier.
