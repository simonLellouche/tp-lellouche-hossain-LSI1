import axios from "axios";

class Product {
  constructor(id = "", name = "", desc = "", price = 0) {
    this.id = id;
    this.name = name;
    this.desc = desc;
    this.price = price;
  }
}

class Stock {
  constructor() {
    this.list_product = [];
  }

  async fetchProducts() {
    try {
      const response = await axios.get("http://localhost:5000/api/products");
      console.log("Products fetched:", response.data.result);
      this.list_product = response.data.result.map((prod) => new Product(prod._id, prod.name, "", prod.price));
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  }

  get_list_product() {
    return this.list_product;
  }

  get_prod_by_id(id) {
    return this.list_product.find((product) => product.id == id) || null;
  }
}

class Cart {
  constructor() {
    this.list_cart = {};
  }

  async fetchCart() {
    try {
      const response = await axios.get("http://localhost:5000/api/cart");
      console.log("Cart fetched:", response.data);
      this.list_cart = response.data.products.reduce((cart, item) => {
        cart[item.productId] = item.quantity;
        return cart;
      }, {});
    } catch (error) {
      console.error("Error fetching cart:", error);
    }
  }

  async addInCart(productId) {
    try {
      await axios.post("http://localhost:5000/api/cart", {
        productId,
        quantity: 1,
      });
      console.log("Added to cart:", productId);
      if (this.list_cart[productId]) {
        this.list_cart[productId]++;
      } else {
        this.list_cart[productId] = 1;
      }
    } catch (error) {
      console.error("Error adding to cart:", error);
    }
  }

  async removeFromCart(productId, removeAll = false) {
    try {
      await axios.delete("http://localhost:5000/api/cart", {
        data: { productId },
      });
      console.log("Removed from cart:", productId);
      if (removeAll) {
        delete this.list_cart[productId];
      } else if (this.list_cart[productId]) {
        if (this.list_cart[productId] === 1) {
          delete this.list_cart[productId];
        } else {
          this.list_cart[productId]--;
        }
      }
    } catch (error) {
      console.error("Error removing from cart:", error);
    }
  }

  get_list_cart() {
    return this.list_cart;
  }

  get_nbr_product() {
    return Object.values(this.list_cart).reduce((sum, quantity) => sum + quantity, 0);
  }

  get_total_price(stock) {
    return Object.entries(this.list_cart).reduce((total, [id, quantity]) => {
      const product = stock.get_prod_by_id(id);
      return total + (product ? product.price * quantity : 0);
    }, 0);
  }
}

export { Product, Stock, Cart };
